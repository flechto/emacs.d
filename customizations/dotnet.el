
(eval-after-load
    'company
  '(add-to-list 'company-backends 'company-omnisharp))

(defun user-csharp-mode-hook ()
  (omnisharp-mode)
  (company-mode))

(add-hook 'csharp-mode-hook 'user-csharp-mode-hook)
