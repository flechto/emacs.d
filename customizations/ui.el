(add-to-list 'custom-theme-load-path "~/.emacs.d/themes")
(load-theme 'tron-legacy t)

(global-auto-revert-mode)
(global-linum-mode)

(tool-bar-mode -1)

(global-company-mode)


(defalias 'yes-or-no-p 'y-or-n-p)

(helm-mode)
(projectile-mode)
(helm-projectile-on)

(global-set-key (kbd "M-x") 'helm-M-x)
(define-key projectile-mode-map (kbd "s-p") 'projectile-command-map)
(define-key projectile-mode-map (kbd "C-c p") 'projectile-command-map)
(global-set-key (kbd "C-x g") 'magit-status)

